import React, { Dispatch, SetStateAction } from 'react';
import { TaskType } from 'types';

export type TasksContextType = {
  tasks: TaskType[];
  setTasks: Dispatch<SetStateAction<TaskType[]>>;
};
export const TasksContext = React.createContext<TasksContextType>(
  {} as TasksContextType
);
