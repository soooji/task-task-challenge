// import userEvent from '@testing-library/user-event';
import { waitFor, within, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import { TaskEditFormType, TaskFormType, TaskStatus } from 'types';
import { ValidTargetStatus } from 'views/Task/config';
import {
  changeInputVal,
  clickOnEl,
  createApp,
  getOptionsOfSelect,
  hasInputValue,
  statusSequence
} from './utils';

// Common Cases
const newTaskCase: TaskFormType = {
  title: 'This is a test title',
  description: 'this is a description of task'
};
const editedTaskCase: TaskEditFormType = {
  title: 'This is a test title - edited-input123',
  description: 'edited-textare123 this is a description of task',
  status: 'InProgress'
};

// Tests
test('check app is running', () => {
  createApp();

  expect(screen.getByText(/Task Management/i)).toBeInTheDocument();
});

test('add new task currectly', async () => {
  // Render APP
  const { getByTestId, getAllByTestId } = createApp();

  // Performm Changes
  const taskTitleInput = getByTestId('title-input');
  const taskDescInput = getByTestId('description-textarea');
  const submitButton = getByTestId('new-task-submit');

  changeInputVal(taskTitleInput, newTaskCase.title);
  changeInputVal(taskDescInput, newTaskCase.description);
  clickOnEl(submitButton);

  // Check
  await waitFor(() => {
    expect(getByTestId('task-item')).toBeInTheDocument();
  });

  const todos = getAllByTestId('task-item');
  const todo = todos[0];

  const { getByTestId: getByTestIdInside } = within(todo);

  expect(getByTestIdInside('task-item-title').textContent).toBe(newTaskCase.title);
  expect(getByTestIdInside('task-item-description').textContent).toBe(
    newTaskCase.description
  );
  expect(getByTestIdInside('task-item-status').textContent).toBe('ToDo');
});

test('task edit form is loaded currectly', async () => {
  // Render APP
  const { getByTestId } = createApp();

  // Performm Changes
  const editTaskBtn = getByTestId('edit-task-button');
  clickOnEl(editTaskBtn);

  // Check Page Change
  await waitFor(() => expect(getByTestId('task-edit-form')).toBeInTheDocument());

  const { getByTestId: getByTestIdWithinForm } = within(
    getByTestId('task-edit-form')
  );

  const taskTitleInput = getByTestIdWithinForm('title-input');
  const taskDescInput = getByTestIdWithinForm('description-textarea');
  const statusSelect = getByTestIdWithinForm('status-select');

  // const submitBtn = getByTestIdWithinForm('task-submit-button');
  const discardBtn = getByTestIdWithinForm('task-discard-button');

  expect(hasInputValue(taskTitleInput, newTaskCase.title)).toBe(true);
  expect(hasInputValue(taskDescInput, newTaskCase.description)).toBe(true);
  expect(hasInputValue(statusSelect, 'ToDo')).toBe(true);

  // Back to home
  clickOnEl(discardBtn);
});

test('task edit form updates task currectly', async () => {
  // Render APP
  const { getByTestId } = createApp();

  // Go to task page
  const editTaskBtn = getByTestId('edit-task-button');
  clickOnEl(editTaskBtn);
  await waitFor(() => expect(getByTestId('task-edit-form')).toBeInTheDocument());

  const { getByTestId: getByTestIdWithinForm } = within(
    getByTestId('task-edit-form')
  );

  const taskTitleInput = getByTestIdWithinForm('title-input');
  const taskDescInput = getByTestIdWithinForm('description-textarea');
  const statusSelect = getByTestIdWithinForm('status-select');
  const submitBtn = getByTestIdWithinForm('task-submit-button');

  // Change
  changeInputVal(taskTitleInput, editedTaskCase.title);
  changeInputVal(taskDescInput, editedTaskCase.description);
  changeInputVal(statusSelect, editedTaskCase.status);

  clickOnEl(submitBtn);

  // Check
  expect(hasInputValue(taskTitleInput, editedTaskCase.title)).toBe(true);
  expect(hasInputValue(taskDescInput, editedTaskCase.description)).toBe(true);
  expect(hasInputValue(statusSelect, editedTaskCase.status)).toBe(true);

  // Back to home
  const discardBtn = getByTestIdWithinForm('task-discard-button');
  clickOnEl(discardBtn);
});

// Test Status Change Sequence
const testStatusChange = (sequnceIndex: number, sequence: TaskStatus[]) => {
  test('check task status flow item', async () => {
    const { getByTestId, getAllByTestId } = createApp();

    // Go to detail page
    const editTaskBtn = getByTestId('edit-task-button');
    clickOnEl(editTaskBtn);
    await waitFor(() => expect(getByTestId('task-edit-form')).toBeInTheDocument());
    const { getByTestId: getByTestIdWithinForm } = within(
      getByTestId('task-edit-form')
    );

    // Options Checking
    const options = getOptionsOfSelect(getAllByTestId, 'status-select-option');
    expect(options).toEqual(ValidTargetStatus[sequence[sequnceIndex]]);

    // Change Status
    const statusSelect = getByTestIdWithinForm('status-select');
    const submitBtn = getByTestIdWithinForm('task-submit-button');

    if (sequnceIndex < sequence.length - 1) {
      changeInputVal(statusSelect, sequence[sequnceIndex + 1]);
      clickOnEl(submitBtn);
    }

    // Back to home
    const discardBtn = getByTestIdWithinForm('task-discard-button');
    clickOnEl(discardBtn);
  });
};
for (let i = 0; i < statusSequence.length; i++) {
  testStatusChange(i, statusSequence);
}

test('remove all tasks', async () => {
  const { getByTestId, getAllByTestId } = createApp();
  expect(getAllByTestId('task-item')[0]).toBeInTheDocument();
  clickOnEl(getByTestId('delete-tasks'));
  expect(getByTestId('empty-tasks')).toBeInTheDocument();
});

//  BeforeAll:
// Ignore Act Warning
const originalError = console.error;
beforeAll(() => {
  jest.spyOn(console, 'error').mockImplementation((...args) => {
    if (
      typeof args[0] === 'string' &&
      args[0].includes('Please upgrade to at least react-dom@16.9.0')
    ) {
      return;
    }
    return originalError.call(console, args);
  });
});
