// import userEvent from '@testing-library/user-event';
import { createMemoryHistory } from 'history';
import {
  fireEvent,
  render,
  RenderResult,
  screen,
  Matcher,
  MatcherOptions
} from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { Router } from 'react-router-dom';
import { TaskStatus } from 'types';

import App from '../App';

// Types
export type TestElement = Window | Document | Node | Element;

// Custom Functions
export const hasInputValue = (e: TestElement, inputValue: string): boolean =>
  screen.getByDisplayValue(inputValue) === e;

export const changeInputVal = (el: TestElement, val: string): void => {
  act(() => {
    fireEvent.change(el, {
      target: {
        value: val
      }
    });
  });
};

export const clickOnEl = (el: TestElement): void => {
  act(() => {
    fireEvent.click(el);
  });
};

export const createApp = (): RenderResult => {
  const history = createMemoryHistory();
  return render(
    <Router history={history}>
      <App />
    </Router>
  );
};

export const getOptionsOfSelect = (
  getAllByTestId: (
    text: Matcher,
    options?: MatcherOptions | undefined,
    waitForElementOptions?: unknown
  ) => HTMLElement[],
  optionTestId: string
): TaskStatus[] => {
  const options = getAllByTestId(optionTestId);
  return options.map((v) => v.textContent as TaskStatus);
};

export const statusSequence: TaskStatus[] = [
  'InProgress',
  'Blocked',
  'ToDo',
  'InProgress',
  'InQA',
  'Done',
  'Deployed'
];
