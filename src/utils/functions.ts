import { SITE_TITLE } from 'utils';

export const makeId = (length: number): string => {
  let result = '';
  const characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
};

export const scrollToTop = (): void => {
  try {
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  } catch {
    //
  }
};

export const getPageTitle = (title?: string): string => {
  return !title ? `${SITE_TITLE}` : `${SITE_TITLE} - ${title}`;
};
