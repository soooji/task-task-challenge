import { TaskStatus } from 'types';
import * as yup from 'yup';

export const taskFormValidation = yup
  .object({
    title: yup.string().required(),
    description: yup.string().required()
  })
  .required();

export const taskEditFormValidation = yup
  .object({
    title: yup.string().required(),
    description: yup.string().required(),
    status: yup.mixed<TaskStatus>().required()
  })
  .required();
