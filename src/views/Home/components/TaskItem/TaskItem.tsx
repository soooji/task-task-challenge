import { FC } from 'react';
import styled from 'styled-components';
import { TaskStatus, TaskType } from 'types/task';
import { Button, Flex, Icon } from 'components';
import { faPen } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';

interface Props extends TaskType {
  className?: string;
  status: TaskStatus;
}

const TaskItem: FC<Props> = (props) => {
  return (
    <Flex
      className={props?.className}
      jc="space-between"
      fd="column"
      data-testid="task-item">
      <div>
        <div className="title" data-testid="task-item-title">
          {props.title}
        </div>
        <p data-testid="task-item-description">{props.description}</p>
      </div>
      <Flex jc="space-between" className="footer">
        <div className="footer__status" data-testid="task-item-status">{props.status}</div>
        <Link to={`/edit/${props.id}`}>
          <Button mode="grey" margin="0" data-testid="edit-task-button">
            <Icon icon={faPen} />
          </Button>
        </Link>
      </Flex>
    </Flex>
  );
};

const StyledTaskItem = styled(TaskItem)`
  height: 100%;
  background: white;
  box-shadow: ${({ theme }) => theme.shadows[3]};
  border-radius: ${({ theme }) => theme.borderRadius.normal};
  padding: ${({ theme }) => theme.space(2)};

  .title {
    font-weight: 500;
    font-size: 1rem;
    color: ${({ theme }) => theme.colors.grey[2]};
  }
  p {
    font-weight: 350;
    font-size: 0.9rem;
    color: ${({ theme }) => theme.colors.grey[4]};
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 5;
    -webkit-box-orient: vertical;
  }
  .footer {
    &__status {
      font-weight: 500;
      font-size: 0.9rem;
      color: white;
      background-color: ${({ theme }) => theme.colors.primary.main};
      padding: ${({ theme }) => theme.space(1)} ${({ theme }) => theme.space(1.5)};
      border-radius: ${({ theme }) => theme.borderRadius.inside};
    }
  }
`;

export default StyledTaskItem;
