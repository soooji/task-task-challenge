import { FC, useContext } from 'react';
import styled from 'styled-components';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';

import { Input, TextArea, Button } from 'components';
import { TaskFormType, TaskType } from 'types';
import { makeId, taskFormValidation } from 'utils';
import { TasksContext, TasksContextType } from 'context';

interface Props {
  className?: string;
}

const NewTask: FC<Props> = (props) => {
  const { tasks, setTasks } = useContext<TasksContextType>(TasksContext);

  const {
    control,
    handleSubmit,
    reset,
    formState: { errors }
  } = useForm<TaskFormType>({
    resolver: yupResolver(taskFormValidation)
  });

  const onSubmit = (data: TaskFormType) => {
    const tempTasks: TaskType[] = [...tasks];

    const newTask: TaskType = {
      ...data,
      id: makeId(7),
      status: 'ToDo',
      createdAt: new Date(),
      history: []
    };

    tempTasks.push(newTask);

    setTasks(tempTasks);

    reset({ title: '', description: '' });
  };

  return (
    <div className={props?.className}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Input name="title" label="Title" errors={errors} control={control} />
        <TextArea
          name="description"
          label="Description"
          errors={errors}
          control={control}
        />
        <br />
        <Button
          width="100%"
          className="submit"
          bold="true"
          margin="0px"
          data-testid="new-task-submit">
          Submit
        </Button>
      </form>
    </div>
  );
};

const StyledNewTask = styled(NewTask)`
  textarea {
    resize: none;
    height: 170px;
  }
  .submit {
    min-height: 45px;
  }
`;

export default StyledNewTask;
