import { FC, useContext } from 'react';
import { Container, Flex, Layout } from 'components';
import { TasksContext, TasksContextType } from 'context';
import styled from 'styled-components';

import { NewTask, TaskItem } from './components';
interface Props {
  className?: string;
}

const Home: FC<Props> = (props) => {
  const { tasks } = useContext<TasksContextType>(TasksContext);

  return (
    <Layout pageTitle="Home">
      <Flex className={props?.className} fd="column">
        <Container className="new-task">
          <NewTask />
        </Container>
        <Flex className="tasks" fd="column">
          <Container className="tasks__title">
            <h2 data-testid="tasks-title">Tasks</h2>
          </Container>
          <Container className="tasks__items">
            <Flex jc="space-between" fw="wrap" ac="stretch" ai="stretch">
              {tasks.length === 0 ? (
                <div className="empty-list" data-testid="empty-tasks">
                  Add a New Task...
                </div>
              ) : (
                tasks.map((v, k) => (
                  <div className="tasks__items__item" key={k}>
                    <TaskItem
                      id={v.id}
                      title={v.title}
                      description={v.description}
                      status={v.status}
                      createdAt={v.createdAt}
                      history={v.history}
                    />
                  </div>
                ))
              )}
            </Flex>
          </Container>
        </Flex>
      </Flex>
    </Layout>
  );
};

const StyledHome = styled(Home)`
  height: calc(100vh - 56px);

  .new-task {
    flex-shrink: 0;
    flex: 0;
  }

  .tasks {
    flex: 1;

    border-radius: ${({ theme }) => theme.borderRadius.outside}
      ${({ theme }) => theme.borderRadius.outside} 0 0;
    background: ${({ theme }) => theme.colors.primary.main};
    margin-top: ${({ theme }) => theme.space(2)};

    &__title {
      flex: 0;
      flex-shrink: 0;
      padding-top: ${({ theme }) => theme.space(2)};
      h2 {
        margin: 0 0 ${({ theme }) => theme.space(2)} 0;
        font-size: 1.2rem;
        color: white;
        font-weight: 500;
      }
    }
    &__items {
      flex: 1 1 1px;
      overflow-y: auto;

      padding-top: ${({ theme }) => theme.space(3)};
      padding-bottom: ${({ theme }) => theme.space(3)};
      background: ${({ theme }) => theme.colors.primary.light};
      border-radius: ${({ theme }) => theme.borderRadius.outside}
        ${({ theme }) => theme.borderRadius.outside} 0 0;
      &__item {
        margin-top: ${({ theme }) => theme.space(2)};
        margin-left: ${({ theme }) => theme.space(1)};
        margin-right: ${({ theme }) => theme.space(1)};
        width: calc(100% / 2 - ${({ theme }) => theme.space(2)});
        &:last-child {
          ${({ theme }) => theme.marginInlineEnd('auto')};
        }
        @media only screen and (max-width: 400px) {
          width: 100%;
        }
      }
    }
  }

  .empty-list {
    text-align: center;
    font-weight: 500;
    color: ${({ theme }) => theme.colors.primary.main};
    font-size: 1.2rem;
    width: 100%;
    margin-left: auto;
    margin-right: auto;
  }
`;

export default StyledHome;
