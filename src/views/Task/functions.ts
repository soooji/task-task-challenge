import { SelectOption } from 'components/Select/Select';
import { TaskHistoryItem, TaskStatus, TaskType } from 'types';
import { ValidTargetStatus } from './config';

export const createHistory = (from: TaskType, to: TaskType): TaskHistoryItem => {
  const item = {
    createdAt: new Date(),
    titleChange: from.title !== to.title,
    descriptionChange: from.description !== to.description,
    statusChange: {
      from: from.status,
      to: to.status
    }
  };
  return item;
};

export const getSuitableOptions = (current: TaskStatus): SelectOption[] => {
  const targets = ValidTargetStatus[current];
  if (!targets) return [];
  return targets.map((v: TaskStatus) => ({ title: v, value: v }));
};

export const isTaskChanged = (item: TaskHistoryItem): boolean => {
  return (
    item.descriptionChange ||
    item.titleChange ||
    item.statusChange.from !== item.statusChange.to
  );
};

export default { createHistory, getSuitableOptions, isTaskChanged };
