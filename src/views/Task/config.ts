import { TaskStatus } from 'types';

export const ValidTargetStatus: {
  ToDo: TaskStatus[];
  InProgress: TaskStatus[];
  Blocked: TaskStatus[];
  InQA: TaskStatus[];
  Done: TaskStatus[];
  Deployed: TaskStatus[];
} = {
  ToDo: ['ToDo', 'InProgress'],
  InProgress: ['InProgress', 'Blocked', 'InQA'],
  Blocked: ['Blocked', 'ToDo'],
  InQA: ['InQA', 'Done', 'ToDo'],
  Done: ['Done', 'Deployed'],
  Deployed: ['Deployed']
};
