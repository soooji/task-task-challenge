import { FC, MouseEvent, useContext, useEffect, useState } from 'react';
import styled from 'styled-components';
import { useForm } from 'react-hook-form';
import { useHistory, useParams } from 'react-router';
import { yupResolver } from '@hookform/resolvers/yup';

import { Layout, Select, Input, TextArea, Container, Button } from 'components';
import { TaskEditFormType, TaskHistoryItem, TaskType } from 'types';
import { TasksContext, TasksContextType } from 'context';
import { taskEditFormValidation } from 'utils';
import { SelectOption } from 'components/Select/Select';
import { createHistory, getSuitableOptions, isTaskChanged } from './functions';
import { TaskHistory } from './components';
import clsx from 'clsx';

interface Props {
  className?: string;
}

const Task: FC<Props> = (props) => {
  // Hooks
  const history = useHistory();
  const { id: taskId } = useParams<{ id: string }>();
  const { tasks, setTasks } = useContext<TasksContextType>(TasksContext);
  const {
    control,
    handleSubmit,
    getValues,
    reset,
    formState: { errors }
  } = useForm<TaskEditFormType>({
    resolver: yupResolver(taskEditFormValidation)
  });

  const [statusOptions, setStatusOptions] = useState<SelectOption[]>([]);
  const [isSucceeded, setIsSucceeded] = useState(false);
  const [currentTaskHistory, setcurrentTaskHistory] = useState<TaskHistoryItem[]>(
    []
  );
  let successMessageTimeOut: NodeJS.Timeout;

  // Life Cycle
  useEffect(() => {
    return () => {
      setIsSucceeded(false);
      clearTimeout(successMessageTimeOut);
    };
  }, []);

  useEffect(() => {
    if (taskId) loadTask(taskId);
  }, [taskId, tasks]);

  // Methods
  const onSubmit = (data: TaskEditFormType) => {
    if (!taskId) return;

    const tempTasks: TaskType[] = [...tasks];
    const index = findTaskIndex(taskId);
    if (index === -1) return;

    const updatedTask: TaskType = {
      ...data,
      id: taskId,
      createdAt: tasks[index].createdAt,
      history: tasks[index].history
    };

    const newHistory: TaskHistoryItem = createHistory(tasks[index], updatedTask);

    if (isTaskChanged(newHistory)) {
      updatedTask.history.push(newHistory);
      tempTasks[index] = updatedTask;
      setTasks(tempTasks);
    }

    showSuccess();
  };

  const loadTask = (id: string) => {
    const index = findTaskIndex(id);
    if (index !== -1) {
      reset(tasks[index]);
      setcurrentTaskHistory(tasks[index].history);
      updateStatusOptions();
    }
  };

  const findTaskIndex = (id: string) => {
    if (!id || !tasks) return -1;
    return tasks.findIndex((t) => t.id === taskId);
  };

  const updateStatusOptions = () => {
    setStatusOptions(getSuitableOptions(getValues('status')));
  };

  const onDiscard = (e: MouseEvent<HTMLButtonElement>) => {
    e.stopPropagation();
    e.preventDefault();
    history.push('/');
  };

  const showSuccess = () => {
    setIsSucceeded(true);
    successMessageTimeOut = setTimeout(() => {
      setIsSucceeded(false);
    }, 3000);
  };

  return (
    <Layout pageTitle="Edit Task" data-testid="task-edit-form">
      <Container className={props?.className}>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Input name="title" label="Title" control={control} errors={errors} />
          <TextArea
            name="description"
            label="Description"
            errors={errors}
            control={control}
          />
          <Select
            name="status"
            label="Status"
            options={statusOptions}
            errors={errors}
            control={control}
          />
          <br />
          <Button
            width="100%"
            className="submit"
            bold="true"
            margin="0px"
            data-testid="task-submit-button">
            Submit
          </Button>
          <Button
            onClick={onDiscard}
            width="100%"
            className="discard"
            margin="0px"
            mode="grey"
            data-testid="task-discard-button">
            Discard
          </Button>

          <div className={clsx('success-message', isSucceeded && '--visible')}>
            Task Successfully Updated!
          </div>

          <br />

          <TaskHistory items={currentTaskHistory} />
        </form>
      </Container>
    </Layout>
  );
};

const StyledTask = styled(Task)`
  textarea {
    resize: vertical;
    min-height: 200px;
  }
  .submit {
    min-height: 45px;
  }
  .discard {
    min-height: 30px;
    margin-top: ${({ theme }) => theme.space(1)};
  }
  .success-message {
    font-size: 0.9rem;
    font-weight: 500;
    text-align: center;
    color: ${({ theme }) => theme.colors.success.main};
    padding: ${({ theme }) => theme.space(1)};
    margin-top: ${({ theme }) => theme.space(1)};
    visibility: hidden;
    opacity: 0;
    &.--visible {
      visibility: visible;
      opacity: 1;
    }
  }

  ul.history {
    font-size: 0.9rem;
    color: ${({ theme }) => theme.colors.grey[3]};
  }
`;

export default StyledTask;
