import { FC } from 'react';
import styled from 'styled-components';
import moment from 'moment';
import { TaskHistoryItem } from 'types';

interface Props {
  className?: string;
  items: TaskHistoryItem[];
}

const TaskHistory: FC<Props> = ({ className, items }) => {
  return (
    <ul className={className}>
      {items
        .slice(0)
        .reverse()
        .map((v, k) => (
          <li key={k}>
            {v.titleChange ? <span>Title Changed, </span> : null}
            {v.descriptionChange ? <span>Description Changed, </span> : null}
            {v.statusChange.from !== v.statusChange.to ? (
              <span>
                from <b>{v.statusChange.from}</b> to <b>{v.statusChange.to}</b> ,
              </span>
            ) : null}
            at <b>{moment(v.createdAt).format('YYYY-MM-DD HH:MM:SS')}</b>
          </li>
        ))}
    </ul>
  );
};

const StyledTaskHistory = styled(TaskHistory)`
  font-size: 0.9rem;
  color: ${({ theme }) => theme.colors.grey[3]};
`;

export default StyledTaskHistory;
