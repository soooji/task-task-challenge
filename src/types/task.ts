export type TaskStatus =
  | 'ToDo'
  | 'InProgress'
  | 'Blocked'
  | 'InQA'
  | 'Done'
  | 'Deployed';

export type TaskFormType = {
  title: string;
  description: string;
};

export type TaskEditFormType = TaskFormType & {
  status: TaskStatus;
};

export type TaskType = {
  id: string;
  createdAt: Date;
  history: TaskHistoryItem[];
} & TaskEditFormType;

export type TaskHistoryItem = {
  createdAt: Date;
  titleChange: boolean;
  descriptionChange: boolean;
  statusChange: {
    from: TaskStatus;
    to: TaskStatus;
  };
};
