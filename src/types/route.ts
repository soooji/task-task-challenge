import { FC } from 'react';

export type RouteItemType = {
  path: string;
  exact?: true;
  component: FC;
};
