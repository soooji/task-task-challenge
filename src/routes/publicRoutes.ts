import { RouteItemType } from 'types';
import { Home, Task } from 'views';

export const publicRoutes: RouteItemType[] = [
  {
    path: '/',
    exact: true,
    component: Home
  },
  {
    path: '/edit/:id',
    exact: true,
    component: Task
  }
];
