import { FC, useContext } from 'react';
import styled from 'styled-components';
import { Flex, Button } from 'components';
import { SITE_TITLE } from 'utils';
import { TasksContext, TasksContextType } from 'context';
import { useHistory } from 'react-router';

interface Props {
  className?: string;
  pageTitle?: string;
}

const Header: FC<Props> = ({ className, pageTitle }) => {
  const { setTasks } = useContext<TasksContextType>(TasksContext);
  const history = useHistory();

  const deleteAllTasks = () => {
    setTasks([]);
    history.push('/');
  };

  return (
    <Flex className={className} jc="space-between">
      <h1>
        {SITE_TITLE} {pageTitle ? ` > ${pageTitle}` : ''}
      </h1>
      <Button onClick={deleteAllTasks} mode="gray" data-testid="delete-tasks">
        Delete Tasks
      </Button>
    </Flex>
  );
};

const StyledHeader = styled(Header)`
  background-color: ${({ theme }) => theme.colors.primary.main};
  min-height: 56px;
  > h1 {
    color: white;
    font-weight: 450;
    margin: 0;
    font-size: 1.3rem;
    padding: ${({ theme }) => theme.space(2)};
  }
`;

export default StyledHeader;
