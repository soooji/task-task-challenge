import { FC, InputHTMLAttributes } from 'react';
import styled from 'styled-components';
import { Field } from 'components';
import { Control, Controller } from 'react-hook-form';

interface Props extends InputHTMLAttributes<HTMLInputElement> {
  className?: string;
  label?: string;
  errors?: {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [key: string]: any;
  };
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  control?: Control<any | Record<string, unknown>>;
}

const Input: FC<Props> = ({
  className,
  label,
  errors,
  control,
  name,
  ...props
}) => {
  return (
    <Controller
      name={name ?? 'INPUT'}
      control={control}
      render={({ field }) => (
        <Field className={className} label={label} errors={errors} name={name}>
          <input
            {...field}
            {...props}
            value={field?.value ?? ''}
            data-testid={`${name}-input`}
          />
        </Field>
      )}
    />
  );
};

const StyledInput = styled(Input)`
  input {
    outline: none;
    border: none;
    width: 100%;
    font-size: 1rem;
    color: ${({ theme }) => theme.colors.grey[2]};

    padding: ${({ theme }) => theme.space(1.5)};
    background-color: ${({ theme }) => theme.colors.grey[8]};
    border-bottom: 1px solid ${({ theme }) => theme.colors.grey[7]};
    transition: ease border-color 0.2s;
    &:focus {
      border-color: ${({ theme }) => theme.colors.primary.main};
    }
  }
`;

export default StyledInput;
