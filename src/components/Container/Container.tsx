import { FC, ReactNode } from 'react';
import styled from 'styled-components';

interface Props {
  children?: ReactNode;
  className?: string;
}

const Container: FC<Props> = ({ className, children }) => {
  return <div className={className}>{children}</div>;
};

const StyledContainer = styled(Container)`
  width: 100%;
  padding-left: ${({ theme }) => theme.space(4)};
  padding-right: ${({ theme }) => theme.space(4)};
`;

export default StyledContainer;
