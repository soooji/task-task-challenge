import { FC, useState, InputHTMLAttributes } from 'react';
import styled from 'styled-components';
import { Field, Icon } from 'components';
import { Control, Controller } from 'react-hook-form';
import { useOutsideClickRef } from 'rooks';
import { faChevronDown, faChevronUp } from '@fortawesome/free-solid-svg-icons';

export type SelectOption = {
  title: string;
  value: string;
};

interface Props extends InputHTMLAttributes<HTMLSelectElement> {
  className?: string;
  label?: string;
  options?: SelectOption[];
  errors?: {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [key: string]: any;
  };
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  control?: Control<any | Record<string, unknown>>;
}

const Input: FC<Props> = ({
  className,
  label,
  errors,
  control,
  name,
  options = [],
  ...props
}) => {
  const [isOpen, setOpen] = useState(false);
  const [selectRef] = useOutsideClickRef(() => setOpen(false));

  return (
    <Controller
      name={name ?? 'INPUT'}
      control={control}
      render={({ field }) => (
        <Field className={className} label={label} errors={errors} name={name}>
          <select {...field} {...props} data-testid={`${name}-select`}>
            {options.map((item, k) => (
              <option key={k} value={item.value} data-testid="status-select-option">
                {item.title}
              </option>
            ))}
          </select>

          <div
            ref={selectRef}
            onClick={() => {
              setOpen((prev) => !prev);
            }}
            className="custom-select-wrapper">
            <div className={`custom-select ${isOpen && 'open'}`}>
              <div className="custom-select__trigger">
                <span>
                  {options.find((item) => item.value === field.value)?.title ||
                    'Select...'}
                </span>
                {isOpen ? (
                  <Icon icon={faChevronUp} />
                ) : (
                  <Icon icon={faChevronDown} />
                )}
              </div>
              <div className="custom-select__options">
                {options.map((item) => (
                  <div
                    key={item.value}
                    onClick={() => {
                      field.onChange(item.value);
                    }}
                    className="custom-select__options__item">
                    <span
                      className={`${field.value === item.value && 'selected'} `}
                      data-value={item.value}>
                      {item.title}
                    </span>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </Field>
      )}
    />
  );
};

const StyledInput = styled(Input)`
  position: relative;

  select {
    display: none;
  }

  .custom-select-wrapper {
    position: relative;
    user-select: none;
    width: 100%;
  }

  .custom-select {
    width: 100%;
    font-size: 1rem;
    transition: ease border-color 0.2s;
    position: relative;
    cursor: pointer;

    color: ${({ theme }) => theme.colors.grey[2]};
    padding: ${({ theme }) => theme.space(1.5)};
    background-color: ${({ theme }) => theme.colors.grey[8]};
    border-bottom: 1px solid ${({ theme }) => theme.colors.grey[7]};

    &__trigger {
      position: relative;
      display: flex;
      align-items: center;
      justify-content: space-between;
      cursor: pointer;
    }

    &__options {
      position: absolute;
      display: block;
      top: 100%;
      left: 0;
      right: 0;
      background: white;
      transition: ease all 0.2s;
      opacity: 0;
      visibility: hidden;
      pointer-events: none;
      z-index: 2;
      max-height: 200px;
      overflow-y: auto;

      box-shadow: ${({ theme }) => theme.shadows[2]};

      &__item {
        span {
          position: relative;
          display: block;
          cursor: pointer;
          transition: ease color 0.2s, ease background-color 0.2s;

          padding: ${({ theme }) => theme.space(1.5)};
          color: ${({ theme }) => theme.colors.grey[3]};
          &.selected {
            color: ${({ theme }) => theme.colors.grey[3]};
            background-color: ${({ theme }) => theme.colors.primary.lighter};
          }
        }
        &:hover {
          span {
            cursor: pointer;
            background-color: ${({ theme }) => theme.colors.grey[7]};
          }
        }
      }
    }
    &.open {
      .custom-select__options {
        opacity: 1;
        visibility: visible;
        pointer-events: all;
      }
    }
  }
`;

export default StyledInput;
