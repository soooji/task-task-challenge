import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import clsx from 'clsx';
import { Icon } from 'components';
import { FC, ReactNode } from 'react';
import styled from 'styled-components';

interface Props {
  children?: ReactNode;
  className?: string;
  name?: string;
  label?: string;
  errors?: {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [key: string]: any;
  };
}

const Field: FC<Props> = ({ children, className, label, errors, name }) => {
  return (
    <div className={className}>
      {label ? <label>{label}</label> : null}
      <div className={clsx('field-container', label && '--has-margin')}>
        {children}
      </div>
      {name && errors?.[name] && errors?.[name]?.message ? (
        <div className="error-text">
          <Icon icon={faInfoCircle} /> {errors[name].message}
        </div>
      ) : null}
    </div>
  );
};

const StyledField = styled(Field)`
  width: 100%;
  margin-top: ${({ theme }) => theme.space(2)};

  label {
    font-size: 0.95rem;
    font-weight: 400;
    color: ${({ theme }) => theme.colors.primary.dark};
    ${({ theme }) => theme.paddingInlineStart(1)};
  }

  .field-container {
    &.--has-margin {
      margin-top: ${({ theme }) => theme.space(1)};
    }
  }

  > .error-text {
    font-size: 0.8rem;
    font-weight: 500;
    color: ${({ theme }) => theme.colors.error.main};
    padding-top: ${({ theme }) => theme.space(1)};
    ${({ theme }) => theme.paddingInlineStart(1)};
  }
`;

export default StyledField;
