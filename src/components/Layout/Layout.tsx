import { Header } from 'components';
import { FC, ReactNode, ReactElement } from 'react';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';
import { getPageTitle } from 'utils';

interface Props {
  children?: ReactNode;
  className?: string;
  header?: ReactElement;
  pageTitle?: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  // [key: string]: any;
}

const Layout: FC<Props> = ({ children, className, pageTitle, ...props }) => {
  return (
    <div className={className} {...props}>
      <Helmet>
        <title>{getPageTitle(pageTitle)}</title>
      </Helmet>
      <Header pageTitle={pageTitle} />
      <div>{children}</div>
    </div>
  );
};

const StyledLayout = styled(Layout)`
  width: 100%;
  max-width: 520px;
  height: 100vh;
  margin-left: auto;
  margin-right: auto;
`;

export default StyledLayout;
