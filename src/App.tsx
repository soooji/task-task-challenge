import { FC, useEffect, useState, useMemo, Suspense } from 'react';
import styled, { ThemeProvider } from 'styled-components';
import { Router, Route, Switch } from 'react-router-dom';

import 'assets/css/normalize/_normalize.scss';
import GlobalStyle from 'GlobalStyle';

import history from 'utils/history';
import theme from 'utils/theme';
import { publicRoutes } from 'routes';
import { TasksContext } from 'context';
import { TaskType } from 'types';
import { Helmet } from 'react-helmet';

interface Props {
  className?: string;
}

const App: FC<Props> = ({ className }) => {
  const getTaskFromLS = (): TaskType[] => {
    try {
      const serializedTasks = localStorage.getItem('TASKS');
      if (!serializedTasks) return [];
      return JSON.parse(serializedTasks) as TaskType[];
    } catch (error) {
      return [];
    }
  };

  const saveTasksToLS = () => {
    try {
      localStorage.setItem('TASKS', JSON.stringify(tasks));
    } catch (error) {
      alert('Error while caching tasks data!');
    }
  };

  const [tasks, setTasks] = useState<TaskType[]>(getTaskFromLS());
  const tasksValue = useMemo(() => ({ tasks, setTasks }), [tasks]);

  useEffect(() => {
    saveTasksToLS();
  }, [tasks]);

  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <TasksContext.Provider value={tasksValue}>
        <Helmet>
          <link
            rel="styleSheet"
            href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap"
          />
        </Helmet>
        <div className={className}>
          <Router history={history}>
            <Suspense fallback={<p>Loading...</p>}>
              <Switch>
                {publicRoutes.map((v) => (
                  <Route
                    key={v.path}
                    path={v.path}
                    exact={v.exact ?? false}
                    component={v.component}
                  />
                ))}
              </Switch>
            </Suspense>
          </Router>
        </div>
      </TasksContext.Provider>
    </ThemeProvider>
  );
};

const StyledApp = styled(App)``;

export default StyledApp;
